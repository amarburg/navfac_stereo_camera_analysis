[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/amarburg%2Fnavfac_stereo_camera_analysis/master?filepath=depth_resolution_analysis.ipynb)

<!-- [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/amarburg%2Fnavfac_stereo_camera_analysis/master) -->

This jupyter notebook is handled by `jupytext` so I don't need to check in the actual notebook.  It needs to be installed to make the notebook.
