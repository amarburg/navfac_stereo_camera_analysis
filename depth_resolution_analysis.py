# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.3.0
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# # Impact of baseline on depth estimation.
#
#

# +
import pandas as pd
import numpy

# %matplotlib inline
import matplotlib.pyplot as plt
import re

# -

# For simplicity, we assume two distortion-free pinhole cameras with the same imaging sensors and focal length `f`.  Assume the camera axes are parallel, and the baseline is only along the +X axis.
#
# ![](camera_image.png)
#
# `b` is the baseline between the cameras.  There are two points on the camera axis of camera `C`: `X` at location Z and `X'` at location Z+dz on its camera axis (for simplicity).
#
# Camera `C` observes bothpoints at the same location (not marked)
#
# Camera `C'` observes the two points at locations `x` and `x'` measured from the camera axis of `C'`
#
# By similar triangles, 
#
# $$ \frac{x}{f} = \frac{b}{Z} $$
# $$ \frac{x`}{f} = \frac{b}{Z+dz} $$
#
# The "disparity" is then the pixel shift $\delta = x' -x$ for a given `dz`:
#
# $$ \delta = fb\left( \frac{1}{Z+dz} - \frac{1}{Z} \right)$$
#
# Note that given this geomertr, $\delta$ will be negative.
#
# Or alternatively, the detectable depth is `dz` as a function of $\delta$:
#
# $$ dz = \frac{1}{ \frac{\delta}{fb} + \frac{1}{z} } - z $$
#
# Minimum detectability occurs when $\left| \delta \right|$ is the pixel pitch of the camera sensor (though practically it's probably ~2-3x ?)
#
# ## Other datapoints
#
# FWIW, human inter-pupil distance averages 6.17cm for woman and 6.4cm for men [Wikipedia](https://en.wikipedia.org/wiki/Pupillary_distance)
#
#  * Stereolabs, both cameras have 4416x1242 sensors (more X pixels):
#      * __Zed mini__: 6.3cm baseline,  0.15-12m quoted working distance
#      * __Zed:__ 12.0cm baseline, 0.2-20m quoted working distance
#

# +
# X resolution of cameras

# Sony IMX264
x_resolution = 2464
x_pitch = 3.45e-6   # 3.45 um
x_width = x_resolution * x_pitch

## 6mm
f = 0.006

print("Lens focal length %f mm" % (f*1000))
print("Sensor is %f m wide" % x_width)

## Horizontal half-FOV is the angle off-center where an point is at the edge of the sensor
b = 1 # doesn't matter
zmax = f*b/(x_width/2)
halffov = numpy.arctan2(b,zmax)
print("Sensor half FOV is %f deg; full horizontal FOV is %f" % (numpy.rad2deg(halffov), numpy.rad2deg(2*halffov)) )

## 1st vision calculator reports  horiz fov of 1.4666 m at 1m working distance
firstvision_halffov = numpy.arctan2( 1.46667/2, 1 )
print("1st vision calculator says %f" % numpy.rad2deg(firstvision_halffov))

# Design baseline
dbaseline = 0.15 #m

print("Using design baseline of %.1f cm" % (dbaseline*100) )


# +
# For a given z, f, b, and pixel pitch, calculate the detectable depth.
def min_detectable( z, f, b, pixel_pitch ):
    # Return NaN if point z would be out of the FOV of camera c'
    x = f*b/z
    if x > x_width/2:
        return NaN
    
    return 1 / ( pixel_pitch/(f*b) +1/z ) - z
    
 ## For a given z, f, b, and dz, calculate the    
def disparity_pix( z, dz, f, b, pixel_pitch ):
    
    return ( f*b*(1/(z+dz) - 1/z ) ) / pixel_pitch


    
#print(min_detectable( 1.0, f, 0.05, x_pitch))
    

data = pd.DataFrame( { 'b': numpy.arange(0.05, 0.5, 0.01) } )
    
## jankiness
z_keys = {}
    
# Negate the x_pitch for positive dz
for z in numpy.arange(1,7,1):
    z_key = "z_%1d_0" % z
    z_keys[z_key] = z
    data[ z_key ] = data['b'].apply( lambda b: min_detectable( z, f, b, -x_pitch ) )

    ## Overlap is horizontal fraction of C' field of view which is shared with C
    data[ "overlap_" + z_key ] = data['b'].apply( lambda b: 1 - ( f*b/(z*x_width) ) )

    data[ "disp_" + z_key ] = data['b'].apply( lambda b: disparity_pix( z, 0.01, f, b, -x_pitch) )
    
## Distance of convergence is the minimum z distance at which point X is visible
#data['doc'] = data['b'].apply( lambda b: 2*f*b/x_width )

print(data.head(5))

# +
fig = plt.figure(figsize=(12,24))
ax = fig.subplots(nrows=3)

for key,z in z_keys.items():
    
    lbl = "z = %.1fm" % z
    
    ax[0].semilogy( data['b'], data[key], label=lbl )

    ax[1].plot( data['b'], data["overlap_" + key], label=lbl)
    
    ax[2].plot( data['b'], data["disp_"+ key], label=lbl )
    
ax[0].set_ylabel('depth sensitivity: dz which results in disparity of one pixel (m)\n blue line at 1cm')
ax[0].axhline(y=0.01, xmin=0, xmax=1, ls="--", lw=1.5, color="blue", alpha=0.3, label='1 cm')

ax[1].set_ylabel('Overlap between sensor images (fraction); red line at 10cm (min distance with 8.6cm acrylic domes)')
    
ax[2].set_ylabel('Disparity for a 1cm depth difference at distance z (pixels)\nblue line at 1 pixel')
ax[2].axhline(y=1.0, xmin=0, xmax=1, ls="--", lw=1.5, color="blue", alpha=0.3, label='1 px')

## This is estimated based on 8.636 OD cm acrylic domes from sexton
## http://www.thesextonco.com/product/acrylic-3-4in-dome-port-with-flange/
min_baseline = 0.1

## Common to both plots
for a in ax:
    a.set_xlabel('Stereo baseline (m)')    
    a.grid(True, ls="--", lw=0.5, color="black", alpha=0.3)
    a.axvline(x=min_baseline, ymin=0, ymax=1, ls="--", lw=1.5, color="red", alpha=0.3)  
    a.axvline(x=dbaseline, ymin=0, ymax=1, ls="--", lw=1.5, color="black", alpha=0.7)  
    a.legend()
    


# +

disparity = pd.DataFrame( { 'z': numpy.arange(0.1, 10.0, 0.1) } )

## 1 cm depth difference
dz = 0.01

bkeys = {}

for b in list(numpy.arange(0.1,0.5,0.1)) + ['design']:

    if b == 'design':
        key='design'
        bkeys[key] = 'design baseline %.1f cm' % (100*dbaseline)
    
        b = dbaseline
    else:
        key = "b_%.2f" % b
        bkeys[key] = "baseline %.1f cm" % (100*b)
            
    disparity["delta_" + key] = disparity['z'].apply( lambda z: disparity_pix( z, dz, f, b, -x_pitch) )

    ## Absolute disparity between images
    disparity[key] = disparity['z'].apply( lambda z: (f*b/z)/x_pitch )
    
print(disparity.head(3))

# +
fig = plt.figure(figsize=(12,24))
ax = fig.subplots(nrows=2)

for key,label in bkeys.items():
    if re.match( "design", label ):
        ax[0].loglog( disparity["delta_" + key], disparity['z'], ls="--", color="black", label=label)
        ax[1].loglog( disparity[key], disparity['z'], ls="--", color="black", label=label)       
    else:
        ax[0].loglog( disparity["delta_" + key], disparity['z'], label=label)
        ax[1].loglog( disparity[key], disparity['z'], label=label)

ax[0].set_xlabel('Disparity from %.1f cm depth difference (pixels)\nred line at 1 pixel' % (dz*100) )
ax[0].axvline(x=1.0, ymin=0, ymax=1, ls="--", lw=1.5, color="red", alpha=0.3)  

ax[1].set_xlabel('Absolute disparity of point on centerline in right camera\nredline at 256 pixels')
ax[1].axvline(x=256, ymin=0, ymax=1, ls="--", lw=1.5, color="red", alpha=0.3)  
    
for a in ax:
    a.set_ylabel('Distance to object (m)')

    a.grid(True, ls="--", lw=0.5, color="black", alpha=0.3)
    
    a.legend()
    


# -




